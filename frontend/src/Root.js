import React, { Fragment } from 'react';
import { isMobile } from 'react-device-detect';

import MobileMessage from 'Components/MobileMessage/MobileMessage';
import App from './App';

const Root = () => <Fragment>{isMobile && window.innerWidth < 700 ? <MobileMessage /> : <App />}</Fragment>;

export default Root;
