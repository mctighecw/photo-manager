const getPrefix = () => {
  const prefix = process.env.NODE_ENV === 'development' ? 'http://localhost:1323/api' : '/api';
  return prefix;
};

export const getUrl = (category, type) => {
  const prefix = getPrefix();
  return `${prefix}/${category}/${type}`;
};
