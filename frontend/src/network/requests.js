import axios from 'axios';
import { getCookie } from 'Utils/cookieFunctions';

const api = axios.create({
  timeout: 10000,
  headers: { 'Content-Type': 'application/json' },
  withCredentials: false,
});

const getTokenHeader = (useToken) => {
  const token = getCookie('token');
  const value = `Bearer ${token}`;
  const res = useToken ? { headers: { Authorization: value } } : {};
  return res;
};

export const get = (url, useToken = true) => {
  const headers = getTokenHeader(useToken);
  return api.get(url, headers);
};

export const post = (url, data, useToken = true) => {
  const headers = getTokenHeader(useToken);
  return api.post(url, data, headers);
};
