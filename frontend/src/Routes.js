import React, { useEffect } from 'react';
import { Switch, Route, Redirect, useHistory } from 'react-router-dom';
import { getCookie } from 'Utils/cookieFunctions';

import Files from 'Components/Files/Files';
import Faces from 'Components/Faces/Faces';
import About from 'Components/About/About';
import NotFound from 'Components/NotFound/NotFound';

const Routes = (props) => {
  const history = useHistory();

  useEffect(() => {
    const token = getCookie('token');

    if (!token) {
      history.push('/login');
    }
  }, []);

  return (
    <Switch>
      <Route path="/files" component={Files} />
      <Route path="/faces" component={Faces} />
      <Route path="/about" component={About} />
      <Route path="/not-found" component={NotFound} />
      <Route exact path="/" render={() => <Redirect to="/files" />} />
      <Route render={() => <Redirect to="/not-found" />} />
    </Switch>
  );
};

export default Routes;
