export const fileTypes = [
  {
    text: 'GIF',
    value: 'gif',
  },
  {
    text: 'JPG',
    value: 'jpg',
  },
  {
    text: 'PNG',
    value: 'png',
  },
];

export const fileCategories = ['Personal', 'Family', 'Travel', 'Leisure', 'Work', 'Other'];
