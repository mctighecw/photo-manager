const aboutLinks = {
  react: { label: 'React', url: 'https://reactjs.org' },
  go: { label: 'Go', url: 'https://golang.org' },
  goFace: { label: 'Go Face', url: 'https://github.com/Kagami/go-face' },
  dlib: { label: 'Dlib', url: 'http://dlib.net' },
  awsS3: { label: 'Amazon Web Services S3', url: 'https://aws.amazon.com/s3' },
  mongodb: { label: 'MongoDB', url: 'https://www.mongodb.com' },
  docker: { label: 'Docker', url: 'https://www.docker.com' },
};

export default aboutLinks;
