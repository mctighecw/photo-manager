import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import Login from 'Components/Login/Login';
import Routes from './Routes';
import './styles/global.css';

const App = () => (
  <Router>
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/" component={Routes} />
      <Route render={() => <Redirect to="/login" />} />
    </Switch>
  </Router>
);

export default App;
