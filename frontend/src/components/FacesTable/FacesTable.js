import React, { useState } from 'react';
import { Table, Pagination, Input, message } from 'antd';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';
import { cleanFileName, bytesToSize, formatDateTime } from 'Utils/functions';

import Actions from './Actions/Actions';
import PhotoModal from 'Components/PhotoModal/PhotoModal';

import './styles.scss';

const FacesTable = (props) => {
  const { faces, getFaces } = props;
  const [photoData, setPhotoData] = useState({});
  const [editMode, setEditMode] = useState(null);
  const [data, setData] = useState({});
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize, setPageSize] = useState(10);

  const handleChangePageNumber = (page, size) => {
    setPageNumber(page);
  };

  const handleChangePageSize = (current, size) => {
    setPageNumber(1);
    setPageSize(size);
  };

  const handleEdit = (record) => {
    if (record === null) {
      setEditMode(null);
      setData({});
    } else {
      const { id, name } = record;

      setEditMode(id);
      setData({
        id,
        name,
      });
    }
  };

  const updateField = (key, value) => {
    const newData = { ...data };

    newData[key] = cleanFileName(value);
    setData(newData);
  };

  const handleUpdate = () => {
    const url = getUrl('faces', 'update');

    if (data.name === '') {
      message.error('Please enter a name');
    } else {
      post(url, data, true)
        .then((res) => {
          handleEdit(null);
          getFaces();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const columns = [
    {
      title: 'Name',
      key: 'name',
      align: 'center',
      sorter: (a, b) => a.name > b.name,
      render: (text, record) => {
        if (editMode === record.id) {
          return (
            <Input
              value={data.name}
              placeholder="Choose a name"
              maxLength={20}
              style={{ width: 120 }}
              onChange={(e) => updateField('name', e.target.value)}
            />
          );
        } else {
          return (
            <div
              styleName="download-link"
              onClick={() =>
                setPhotoData({
                  hashedName: record.hashed_name,
                  originalName: record.name,
                  extension: record.extension,
                })
              }
            >
              {record.name}
            </div>
          );
        }
      },
    },
    {
      title: 'Type',
      dataIndex: 'extension',
      key: 'extension',
      align: 'center',
      width: '120px',
      sorter: (a, b) => a.extension - b.extension,
    },
    {
      title: 'Size',
      key: 'size',
      align: 'center',
      sorter: (a, b) => a.size > b.size,
      render: (text, record) => bytesToSize(record.size),
    },
    {
      title: 'Uploaded',
      dataIndex: 'created_at',
      key: 'created_at',
      align: 'center',
      sorter: (a, b) => a.created_at > b.created_at,
      render: (text, record) => formatDateTime(record.created_at),
    },
    {
      title: 'Actions',
      key: 'actions',
      align: 'center',
      render: (text, record) => (
        <Actions
          record={record}
          editMode={editMode}
          getFaces={getFaces}
          handleEdit={handleEdit}
          handleUpdate={handleUpdate}
        />
      ),
    },
  ];

  const start = (pageNumber - 1) * pageSize;
  const end = pageNumber * pageSize;
  const displayFaces = faces.slice(start, end);

  return (
    <div styleName="container">
      <Table columns={columns} dataSource={displayFaces} rowKey="id" bordered={true} pagination={false} />

      <div styleName="pagination">
        <Pagination
          showQuickJumper
          defaultCurrent={1}
          defaultPageSize={10}
          pageSizeOptions={['10', '20', '50']}
          showSizeChanger={true}
          current={pageNumber}
          pageSize={pageSize}
          total={faces.length}
          onChange={handleChangePageNumber}
          onShowSizeChange={handleChangePageSize}
        />
      </div>

      {Object.keys(photoData).length > 0 && (
        <PhotoModal photoData={photoData} handleClose={() => setPhotoData({})} />
      )}
    </div>
  );
};

export default FacesTable;
