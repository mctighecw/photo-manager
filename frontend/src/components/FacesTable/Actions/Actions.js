import React from 'react';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';
import { Popconfirm, Button, message } from 'antd';

import { DownloadOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import './styles.scss';

const Actions = (props) => {
  const { record, editMode, getFaces, handleEdit, handleUpdate } = props;
  const { id, hashed_name, name, extension } = record;
  const fileName = name + '.' + extension;

  const handleDownload = () => {
    const url = getUrl('file', 'download');
    const data = { hashed_name, extension };

    post(url, data, true)
      .then((res) => {
        const uri = `data:image/${extension};base64,${res.data.file}`;
        let link = document.createElement('a');
        link.download = fileName;
        link.href = uri;
        link.click();
        message.success(`${fileName} has been downloaded`);
      })
      .catch((err) => {
        console.log(err);
        message.error(`Error downloading ${fileName}`);
      });
  };

  const handleDelete = () => {
    const url = getUrl('faces', 'delete');
    const data = { id };

    post(url, data, true)
      .then((res) => {
        message.success(`${fileName} has been deleted`);
        getFaces();
      })
      .catch((err) => {
        message.error(`Error deleting ${fileName}`);
      });
  };

  return (
    <div styleName="container">
      {editMode !== null && editMode === id ? (
        <div styleName="actions buttons">
          <Button type="primary" size="small" onClick={() => handleEdit(null)}>
            Cancel
          </Button>

          <Button type="primary" size="small" onClick={() => handleUpdate()}>
            Save
          </Button>
        </div>
      ) : (
        <div styleName="actions icons">
          <Popconfirm
            title="Download this file?"
            onConfirm={(e) => handleDownload()}
            onCancel={null}
            okText="Download"
            cancelText="Cancel"
          >
            <DownloadOutlined />
          </Popconfirm>

          <Popconfirm
            title="Edit this file?"
            onConfirm={(e) => handleEdit(record)}
            onCancel={null}
            okText="Edit"
            cancelText="Cancel"
          >
            <EditOutlined />
          </Popconfirm>

          <Popconfirm
            title="Delete this file?"
            onConfirm={(e) => handleDelete()}
            onCancel={null}
            okText="Delete"
            cancelText="Cancel"
          >
            <DeleteOutlined />
          </Popconfirm>
        </div>
      )}
    </div>
  );
};

export default Actions;
