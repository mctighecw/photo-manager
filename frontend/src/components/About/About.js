import React, { Fragment } from 'react';
import TopMenu from 'Components/TopMenu/TopMenu';
import Footer from './Footer/Footer';

import reactGopher from 'Assets/gopher-react.png';
import links from 'Constants/aboutLinks';
import './styles.scss';

const About = () => {
  const renderLink = (name) => {
    const link = links[name];

    return (
      <a href={link.url} target="_blank">
        {link.label}
      </a>
    );
  };

  return (
    <Fragment>
      <TopMenu />
      <div styleName="content">
        <div styleName="top-section">
          <div styleName="heading">About</div>

          <div styleName="info">
            <img src={reactGopher} alt="Gopher" styleName="gopher" />
            <div styleName="text">
              Photo Manager has a {renderLink('react')} frontend and a {renderLink('go')} backend.{' '}
              {renderLink('goFace')}, a facial recognition package for Go, applies a popular machine learning
              library, {renderLink('dlib')}, to detect faces in photos. The photo files are saved to an{' '}
              {renderLink('awsS3')} bucket and photo data are saved to a {renderLink('mongodb')} database. The
              app runs from {renderLink('docker')} containers deployed in the Cloud.
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </Fragment>
  );
};

export default About;
