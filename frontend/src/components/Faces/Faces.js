import React, { useState, useEffect, Fragment } from 'react';
import { getUrl } from 'Network/urls';
import { get } from 'Network/requests';

import TopMenu from 'Components/TopMenu/TopMenu';
import UploadFace from 'Components/UploadFace/UploadFace';
import FacesTable from 'Components/FacesTable/FacesTable';
import './styles.scss';

const Faces = () => {
  const [faces, setFaces] = useState([]);

  const getFaces = () => {
    const url = getUrl('faces', 'all');

    get(url, true)
      .then((res) => {
        setFaces(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getFaces();
  }, []);

  return (
    <Fragment>
      <TopMenu />
      <div styleName="content">
        <div styleName="heading">Faces</div>
        <UploadFace getFaces={getFaces} />

        <div styleName="heading">Library</div>
        <FacesTable faces={faces} getFaces={getFaces} />
      </div>
    </Fragment>
  );
};

export default Faces;
