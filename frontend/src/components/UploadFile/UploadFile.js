import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';

import { Button, message } from 'antd';
import AddFileIcon from 'Assets/add-file.svg';
import LoadingSpinner from 'Assets/loading-spinner.svg';
import './styles.scss';

const UploadFile = (props) => {
  const { getFiles } = props;
  const [uploading, setUploading] = useState(false);
  const [files, setFiles] = useState(null);

  const onDrop = (data) => {
    const formData = new FormData();
    formData.append('length', data.length);

    if (data.length > 1) {
      data.forEach((item, index) => {
        formData.append(`file${index}`, item);
      });
    } else {
      formData.append('file', data[0]);
    }
    setFiles(formData);
  };

  const uploadAttachment = () => {
    const url = getUrl('file', 'upload');

    setUploading(true);

    if (files !== null) {
      post(url, files, true)
        .then((res) => {
          setFiles(null);
          setUploading(false);
          getFiles();
          message.success(res.data.message);
        })
        .catch((err) => {
          console.log(err);
          setUploading(false);

          if (err.response.status === 413) {
            message.error('Upload file is too large (2 MB limit)', 4);
          } else {
            message.error('Error uploading file');
          }
        });
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/png, image/gif, image/jpeg',
    multiple: true,
    onDrop,
  });

  return (
    <div styleName="container">
      <div styleName="drop-zone">
        <div {...getRootProps()}>
          <input {...getInputProps()} />
          <img src={AddFileIcon} alt="Add" />
          {isDragActive ? <p>Drop files here</p> : <p>Upload PNG, JPG or GIF files</p>}
        </div>
      </div>

      {files !== null && (
        <div styleName="content-right">
          <Button type="primary" size="middle" onClick={() => uploadAttachment()}>
            Upload
          </Button>

          <div styleName="spacer" />

          <Button danger onClick={() => setFiles(null)}>
            Remove
          </Button>
        </div>
      )}

      {uploading && (
        <div styleName="content-right">
          <img src={LoadingSpinner} alt="Spinner" />
          <div styleName="text">Uploading...</div>
        </div>
      )}
    </div>
  );
};

export default UploadFile;
