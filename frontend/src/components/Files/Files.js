import React, { useState, useEffect, Fragment } from 'react';
import { getUrl } from 'Network/urls';
import { get } from 'Network/requests';

import TopMenu from 'Components/TopMenu/TopMenu';
import UploadFile from 'Components/UploadFile/UploadFile';
import FileTable from 'Components/FileTable/FileTable';

import './styles.scss';

const Files = (props) => {
  const [files, setFiles] = useState([]);

  const getFiles = () => {
    const url = getUrl('file', 'all');

    get(url, true)
      .then((res) => {
        setFiles(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getFiles();
  }, []);

  return (
    <Fragment>
      <TopMenu />
      <div styleName="content">
        <div styleName="heading">Files</div>
        <UploadFile getFiles={getFiles} />

        <div styleName="heading">Library</div>
        <FileTable files={files} getFiles={getFiles} />
      </div>
    </Fragment>
  );
};

export default Files;
