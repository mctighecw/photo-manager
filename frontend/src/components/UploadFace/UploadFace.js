import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';

import { Input, Button, message } from 'antd';
import AddFileIcon from 'Assets/add-file.svg';
import LoadingSpinner from 'Assets/loading-spinner.svg';
import './styles.scss';

const UploadFace = (props) => {
  const { getFaces } = props;
  const [uploading, setUploading] = useState(false);
  const [file, setFile] = useState(null);
  const [name, setName] = useState('');

  const onDrop = (data) => {
    if (data.length === 1) {
      const formData = new FormData();
      formData.append('file', data[0]);
      setFile(formData);
    } else {
      message.error('Only one file can be uploaded');
    }
  };

  const handleClear = () => {
    setFile(null);
    setName('');
  };

  const uploadAttachment = () => {
    const url = getUrl('faces', 'upload');
    const data = file;
    data.append('name', name);

    setUploading(true);

    if (file !== null) {
      post(url, data, true)
        .then((res) => {
          setUploading(false);

          if (res.data.status === 'Error') {
            message.error(res.data.message, 4);
          } else {
            handleClear();
            getFaces();
            message.success(res.data.message);
          }
        })
        .catch((err) => {
          console.log(err);
          setUploading(false);

          if (err.response.status === 413) {
            message.error('Upload file is too large (1 MB limit)', 4);
          } else {
            message.error('Error uploading file');
          }
        });
    }
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    accept: 'image/png, image/gif, image/jpeg',
    multiple: false,
    onDrop,
  });

  return (
    <div styleName="container">
      <div styleName="row">
        <div styleName="description">
          Upload a photo with one face in it and provide the person's name.
          <br />
          The face data in the faces library below will be used to identify people in newly uploaded photo
          files.
        </div>
      </div>

      <div styleName="row margin-bottom">
        <div styleName="drop-zone">
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            <img src={AddFileIcon} alt="Add" />
            {isDragActive ? <p>Drop file here</p> : <p>Upload a PNG, JPG or GIF file</p>}
          </div>
        </div>
      </div>

      {file !== null && (
        <div styleName="row margin-bottom">
          <Input
            size="large"
            placeholder="Name of person in photo"
            value={name}
            style={{ width: 300 }}
            onChange={(e) => setName(e.target.value)}
          />

          {uploading && (
            <div styleName="content-right">
              <img src={LoadingSpinner} alt="Spinner" />
              <div styleName="text">Uploading...</div>
            </div>
          )}
        </div>
      )}

      {file !== null && name !== '' && (
        <div styleName="row">
          <div styleName="buttons">
            <Button
              type="primary"
              size="middle"
              style={{ width: 145, marginRight: 10 }}
              onClick={() => uploadAttachment()}
            >
              Upload
            </Button>

            <Button danger style={{ width: 145 }} onClick={handleClear}>
              Clear
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default UploadFace;
