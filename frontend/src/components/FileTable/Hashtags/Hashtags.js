import React from 'react';
import { Tag } from 'antd';
import { checkNullArray, stringToHexColor } from 'Utils/functions';
import { fileCategories } from 'Constants/table';
import EditTags from './EditTags/EditTags';

const Hashtags = (props) => {
  const { editMode, record, updateField } = props;
  const hashtags = checkNullArray(record.hashtags);
  let res;

  if (editMode === record.id) {
    res = <EditTags hashtags={hashtags} handleChange={(values) => updateField('hashtags', values)} />;
  } else {
    if (hashtags.length > 0) {
      res = [];

      hashtags.map((tag, index) => {
        const color = stringToHexColor(tag);

        res.push(
          <Tag key={index} color={color} style={{ margin: 5 }}>
            {tag}
          </Tag>
        );
      });
    } else {
      res = 'None';
    }
  }

  return res;
};

export default Hashtags;
