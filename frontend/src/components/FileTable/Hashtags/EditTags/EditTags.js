import React, { Fragment } from 'react';
import { Tag, Input, message } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { PlusOutlined } from '@ant-design/icons';
import { cleanHashtag } from 'Utils/functions';
import './styles.scss';

class EditTags extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tags: props.hashtags,
      inputVisible: false,
      inputValue: '',
    };
  }

  handleRemove = (removedTag) => {
    const tags = this.state.tags.filter((tag) => tag !== removedTag);
    this.setState({ tags });
    this.props.handleChange(tags);
  };

  showInput = () => {
    if (this.state.tags.length < 5) {
      this.setState({ inputVisible: true }, () => this.input.focus());
    } else {
      message.error('Maximum 5 hashtags per photo');
    }
  };

  handleInputChange = (e) => {
    const value = cleanHashtag(e.target.value);
    this.setState({ inputValue: value });
  };

  handleInputConfirm = () => {
    const { inputValue } = this.state;
    let tags = this.state.tags;

    if (inputValue && tags.indexOf(inputValue) === -1) {
      tags = [...tags, inputValue];
    }

    this.setState({
      tags,
      inputVisible: false,
      inputValue: '',
    });

    this.props.handleChange(tags);
  };

  saveInputRef = (input) => (this.input = input);

  forMap = (tag) => {
    const tagElem = (
      <Tag
        closable
        onClose={(e) => {
          e.preventDefault();
          this.handleRemove(tag);
        }}
      >
        {tag}
      </Tag>
    );

    return (
      <span key={tag} style={{ display: 'inline-block' }}>
        {tagElem}
      </span>
    );
  };

  render() {
    const { tags, inputVisible, inputValue } = this.state;
    const tagChild = tags.map(this.forMap);

    return (
      <Fragment>
        <div style={{ marginBottom: 16 }}>
          <TweenOneGroup
            enter={{
              scale: 0.8,
              opacity: 0,
              type: 'from',
              duration: 100,
              onComplete: (e) => {
                e.target.style = '';
              },
            }}
            leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
            appear={false}
          >
            {tagChild}
          </TweenOneGroup>
        </div>

        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="small"
            maxLength={12}
            style={{ width: 85 }}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
          />
        )}

        {!inputVisible && (
          <Tag onClick={this.showInput} styleName="site-tag-plus">
            <PlusOutlined /> New Tag
          </Tag>
        )}
      </Fragment>
    );
  }
}

export default EditTags;
