import React, { useState, useRef } from 'react';
import { Table, Pagination, Input, Select, Button, Space, Tag, message } from 'antd';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';

import {
  cleanFileName,
  bytesToSize,
  formatDateTime,
  checkNullArray,
  renderCommaSeparatedItems,
} from 'Utils/functions';

import { fileTypes, fileCategories } from 'Constants/table';

import Hashtags from './Hashtags/Hashtags';
import Actions from './Actions/Actions';
import FacesPanel from './FacesPanel/FacesPanel';
import PhotoModal from 'Components/PhotoModal/PhotoModal';

const { Option } = Select;

import { SearchOutlined, UpOutlined, DownOutlined } from '@ant-design/icons';
import { NAVY_BLUE, HIGHLIGHT_RED } from '../../styles/colors';
import './styles.scss';
import './styles.less';

const FileTable = (props) => {
  const { files, getFiles } = props;
  const [photoData, setPhotoData] = useState({});
  const [editMode, setEditMode] = useState(null);
  const [data, setData] = useState({});
  const [searchTag, setSearchTag] = useState('');
  const searchInput = useRef(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [expandedRow, setExpandedRow] = useState([]);

  const handleChangePageNumber = (page, size) => {
    setPageNumber(page);
  };

  const handleChangePageSize = (current, size) => {
    setPageNumber(1);
    setPageSize(size);
  };

  const handleSearch = (selectedKeys, confirm) => {
    confirm();
    setSearchTag(selectedKeys[0]);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchTag('');
  };

  const handleEdit = (record) => {
    if (record === null) {
      setEditMode(null);
      setData({});
    } else {
      const { id, original_name, category, hashtags } = record;

      setEditMode(id);
      setData({
        id,
        original_name,
        category,
        hashtags: checkNullArray(hashtags),
      });
    }
  };

  const updateField = (key, value) => {
    const newData = { ...data };

    if (key === 'original_name') {
      newData[key] = cleanFileName(value);
    } else {
      newData[key] = value;
    }
    setData(newData);
  };

  const handleUpdate = () => {
    const url = getUrl('file', 'info');

    if (data.original_name === '') {
      message.error('Please enter a filename');
    } else {
      post(url, data, true)
        .then((res) => {
          handleEdit(null);
          getFiles();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const columns = [
    {
      title: 'Name',
      key: 'original_name',
      align: 'center',
      sorter: (a, b) => a.original_name > b.original_name,
      render: (text, record) => {
        if (editMode === record.id) {
          return (
            <Input
              value={data.original_name}
              placeholder="Choose a filename"
              maxLength={20}
              style={{ width: 120 }}
              onChange={(e) => updateField('original_name', e.target.value)}
            />
          );
        } else {
          return (
            <div
              styleName="download-link"
              onClick={() =>
                setPhotoData({
                  hashedName: record.hashed_name,
                  originalName: record.original_name,
                  extension: record.extension,
                })
              }
            >
              {record.original_name}
            </div>
          );
        }
      },
    },
    {
      title: 'Type',
      dataIndex: 'extension',
      key: 'extension',
      align: 'center',
      width: '120px',
      filters: fileTypes,
      sorter: (a, b) => a.extension - b.extension,
      onFilter: (value, record) => record.extension.includes(value),
    },
    {
      title: 'Category',
      key: 'category',
      align: 'center',
      sorter: (a, b) => a.category > b.category,
      render: (text, record) => {
        if (editMode === record.id) {
          const value = data.category === '' ? 'Select one' : data.category;

          const optionsArray = fileCategories.map((item, index) => (
            <Option key={index} value={item}>
              {item}
            </Option>
          ));

          return (
            <Select value={value} style={{ width: 130 }} onChange={(value) => updateField('category', value)}>
              {optionsArray}
            </Select>
          );
        } else {
          if (record.category !== '') {
            return record.category;
          } else {
            return 'None';
          }
        }
      },
    },
    {
      title: 'Hashtags',
      key: 'hashtags',
      align: 'center',
      className: 'hashtags-column',
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div style={{ padding: 8 }}>
          <Input
            ref={searchInput}
            value={selectedKeys[0]}
            placeholder="Search hashtags"
            onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={() => handleSearch(selectedKeys, confirm)}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Space>
            <Button
              onClick={() => handleReset(clearFilters)}
              type="link"
              size="small"
              disabled={searchTag === ''}
              style={{ width: 90 }}
            >
              Reset
            </Button>
            <Button
              type="primary"
              onClick={() => handleSearch(selectedKeys, confirm)}
              icon={<SearchOutlined />}
              size="small"
              style={{ width: 90 }}
            >
              Search
            </Button>
          </Space>
        </div>
      ),
      filterIcon: (filtered) => <SearchOutlined style={{ color: filtered ? NAVY_BLUE : 'inherit' }} />,
      onFilter: (value, record) => record['hashtags'].includes(value.toLowerCase()),
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => searchInput.current.select());
        }
      },
      render: (text, record) => {
        if (searchTag !== '') {
          if (record.hashtags.includes(searchTag.toLowerCase())) {
            return <Tag color={HIGHLIGHT_RED}>{searchTag.toLowerCase()}</Tag>;
          }
        } else {
          return <Hashtags record={record} editMode={editMode} updateField={updateField} />;
        }
      },
    },
    {
      title: 'Faces',
      key: 'faces',
      align: 'center',
      render: (text, record) => renderCommaSeparatedItems(record.faces),
    },
    {
      title: 'Size',
      key: 'size',
      align: 'center',
      sorter: (a, b) => a.size > b.size,
      render: (text, record) => bytesToSize(record.size),
    },
    {
      title: 'Uploaded',
      dataIndex: 'created_at',
      key: 'created_at',
      align: 'center',
      sorter: (a, b) => a.created_at > b.created_at,
      render: (text, record) => formatDateTime(record.created_at),
    },
    {
      title: 'Actions',
      key: 'actions',
      align: 'center',
      render: (text, record) => (
        <Actions
          record={record}
          editMode={editMode}
          getFiles={getFiles}
          handleEdit={handleEdit}
          handleUpdate={handleUpdate}
        />
      ),
    },
  ];

  const renderExpandIcon = (expanded, onExpand, record) => {
    if (expanded) {
      return <UpOutlined style={{ color: 'grey' }} onClick={(e) => onExpand(record)} />;
    } else {
      return <DownOutlined style={{ color: 'grey' }} onClick={(e) => onExpand(record)} />;
    }
  };

  const changeExpandedRow = (rows) => {
    let value = rows;

    // Only allow one row to be expanded
    if (rows.length > 1) {
      value = rows.slice(1);
    }
    setExpandedRow(value);
  };

  const start = (pageNumber - 1) * pageSize;
  const end = pageNumber * pageSize;
  const displayFiles = files.slice(start, end);

  return (
    <div styleName="container">
      <Table
        columns={columns}
        dataSource={displayFiles}
        rowKey="id"
        bordered={true}
        pagination={false}
        expandable={{
          expandIconColumnIndex: 5,
          expandedRowKeys: expandedRow,
          expandIcon: ({ expanded, onExpand, record }) => renderExpandIcon(expanded, onExpand, record),
          onExpandedRowsChange: (expandedRows) => changeExpandedRow(expandedRows),
          expandedRowRender: (record) => (
            <FacesPanel record={record} mounted={expandedRow.includes(record.id)} getFiles={getFiles} />
          ),
        }}
      />

      <div styleName="pagination">
        <Pagination
          showQuickJumper
          defaultCurrent={1}
          defaultPageSize={10}
          pageSizeOptions={['10', '20', '50', '100']}
          showSizeChanger={true}
          current={pageNumber}
          pageSize={pageSize}
          total={files.length}
          onChange={handleChangePageNumber}
          onShowSizeChange={handleChangePageSize}
        />
      </div>

      {Object.keys(photoData).length > 0 && (
        <PhotoModal photoData={photoData} handleClose={() => setPhotoData({})} />
      )}
    </div>
  );
};

export default FileTable;
