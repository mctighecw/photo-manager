import React, { useState } from 'react';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';
import { Button, message } from 'antd';
import EditNames from './EditNames/EditNames';
import './styles.scss';

const AddManually = (props) => {
  const { record, setManually, getFiles } = props;
  const [names, setNames] = useState([...record.faces]);

  const handleUpdate = () => {
    const url = getUrl('file', 'faces');
    const data = { id: record.id, faces: names };

    post(url, data, true)
      .then((res) => {
        getFiles();
        setManually(false);
        message.success('Names have been saved');
      })
      .catch((err) => {
        console.log(err);
        message.error('Error saving names');
      });
  };

  return (
    <div styleName="container">
      <div styleName="heading">Manual Entry</div>
      <EditNames record={record} names={names} setNames={setNames} />

      <div styleName="buttons">
        <Button size="middle" style={{ width: 140 }} onClick={() => setManually(false)}>
          Back
        </Button>
        <Button type="primary" size="middle" style={{ width: 140 }} onClick={() => handleUpdate()}>
          Save Names
        </Button>
      </div>
    </div>
  );
};

export default AddManually;
