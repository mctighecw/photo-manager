import React from 'react';
import { Tag, Input } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import { PlusOutlined } from '@ant-design/icons';
import './styles.scss';

class EditNames extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputVisible: false,
      inputValue: '',
    };
  }

  handleRemove = (removedName) => {
    const names = this.props.names.filter((name) => name !== removedName);
    this.props.setNames(names);
  };

  showInput = () => {
    this.setState({ inputVisible: true }, () => this.input.focus());
  };

  handleInputChange = (e) => {
    this.setState({ inputValue: e.target.value });
  };

  handleInputConfirm = () => {
    const { inputValue } = this.state;
    let names = this.props.names;

    if (inputValue && names.indexOf(inputValue) === -1) {
      names = [...names, inputValue];
    }

    this.setState({
      inputVisible: false,
      inputValue: '',
    });

    this.props.setNames(names);
  };

  saveInputRef = (input) => (this.input = input);

  forMap = (name) => {
    const nameElem = (
      <Tag
        closable
        onClose={(e) => {
          e.preventDefault();
          this.handleRemove(name);
        }}
      >
        {name}
      </Tag>
    );

    return (
      <span key={name} styleName="tag-box">
        {nameElem}
      </span>
    );
  };

  render() {
    const { names } = this.props;
    const { inputVisible, inputValue } = this.state;
    const nameChild = names.map(this.forMap);

    return (
      <div styleName="container">
        <div styleName="names-boxes">
          <TweenOneGroup
            enter={{
              scale: 0.8,
              opacity: 0,
              type: 'from',
              duration: 100,
              onComplete: (e) => {
                e.target.style = '';
              },
            }}
            leave={{ opacity: 0, width: 0, scale: 0, duration: 200 }}
            appear={false}
          >
            {nameChild}
          </TweenOneGroup>
        </div>

        {inputVisible && (
          <Input
            ref={this.saveInputRef}
            type="text"
            size="small"
            maxLength={12}
            style={{ width: 85 }}
            value={inputValue}
            onChange={this.handleInputChange}
            onBlur={this.handleInputConfirm}
            onPressEnter={this.handleInputConfirm}
          />
        )}

        {!inputVisible && (
          <Tag onClick={this.showInput} styleName="site-name-plus">
            <PlusOutlined /> Add Name
          </Tag>
        )}
      </div>
    );
  }
}

export default EditNames;
