import React, { useState, useEffect, Fragment } from 'react';
import { Button, Popconfirm, message } from 'antd';
import { checkNullArray, renderCommaSeparatedItems } from 'Utils/functions';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';
import './styles.scss';

const FaceRecognition = (props) => {
  const { record, mounted, getFiles, setManually } = props;
  const { id, faces, hashed_name, extension } = record;
  const facesArray = checkNullArray(faces);
  const filename = `${hashed_name}.${extension}`;
  const [count, setSetcount] = useState(null);

  const handleDownload = () => {
    const url = getUrl('faces', 'download');
    const data = { hashed_name, extension };

    post(url, data, true)
      .then((res) => {
        handleCountFaces();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleRemove = () => {
    const url = getUrl('faces', 'remove');
    const data = { hashed_name, extension };

    post(url, data, true)
      .then((res) => {})
      .catch((err) => {
        console.log(err);
      });
  };

  const handleCountFaces = () => {
    const url = getUrl('faces', 'count');
    const data = { filename };

    post(url, data, true)
      .then((res) => {
        const { count } = res.data;
        setSetcount(count);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleClearFaces = () => {
    const url = getUrl('faces', 'clear');
    const data = { id };

    post(url, data, true)
      .then((res) => {
        getFiles();
        message.success('Faces have been cleared');
      })
      .catch((err) => {
        console.log(err);
        message.error('Error clearing faces');
      });
  };

  const handleIdentifyFaces = () => {
    const url = getUrl('faces', 'identify');
    const data = { id, filename };

    post(url, data, true)
      .then((res) => {
        if (res.data.status && res.data.status === 'OK') {
          const { count } = res.data;
          const facesMsg = count === 1 ? '1 face' : `${count} faces`;
          getFiles();
          message.success(`${facesMsg} identified successfully`);
        } else {
          message.error('Error identifying face(s)');
        }
      })
      .catch((err) => {
        console.log(err);
        message.error('Error identifying face(s)');
      });
  };

  useEffect(() => {
    if (mounted && facesArray.length === 0) {
      handleDownload();
    } else {
      handleRemove();
    }
  }, [mounted, facesArray.length]);

  return (
    <div styleName="container">
      <div styleName="heading">Face Names</div>

      {facesArray.length === 0 && count === null && <div styleName="message italic">Loading...</div>}

      {facesArray.length === 0 && count !== null && (
        <Fragment>
          <div styleName="message">
            {count > 0 && <div>Faces have been found but haven't been identified.</div>}
          </div>
          <div styleName="row">
            <div>Number of faces found:</div>
            <div>{count}</div>
          </div>

          <div styleName="buttons">
            <Button size="middle" onClick={() => setManually(true)}>
              Enter Manually
            </Button>
            <Button type="primary" size="middle" disabled={count === 0} onClick={() => handleIdentifyFaces()}>
              Identify Faces
            </Button>
          </div>
        </Fragment>
      )}

      {facesArray.length > 0 && (
        <Fragment>
          <div styleName="row">
            <div>Faces:</div>
            <div styleName="names">{renderCommaSeparatedItems(faces)}</div>
          </div>
          <div styleName="buttons">
            <Button size="middle" style={{ width: 140 }} onClick={() => setManually(true)}>
              Enter Manually
            </Button>

            <Popconfirm
              placement="top"
              title="Are you sure that you want to clear the faces in this photo?"
              okText="Clear"
              cancelText="Cancel"
              onConfirm={() => handleClearFaces()}
            >
              <Button danger size="middle" style={{ width: 140 }}>
                Clear
              </Button>
            </Popconfirm>
          </div>
        </Fragment>
      )}
    </div>
  );
};

export default FaceRecognition;
