import React, { useState } from 'react';
import AddManually from './AddManually/AddManually';
import FaceRecognition from './FaceRecognition/FaceRecognition';
import './styles.scss';

const FacesPanel = (props) => {
  const { record, mounted, getFiles } = props;
  const [manually, setManually] = useState(false);

  return (
    <div styleName="container">
      {manually ? (
        <AddManually record={record} getFiles={getFiles} setManually={setManually} />
      ) : (
        <FaceRecognition record={record} mounted={mounted} getFiles={getFiles} setManually={setManually} />
      )}
    </div>
  );
};

export default FacesPanel;
