import React from 'react';
import { useHistory } from 'react-router-dom';
import gopher404 from 'Assets/gopher-404.png';
import './styles.scss';

const NotFound = (props) => {
  const history = useHistory();

  const clickReturn = () => {
    history.push('/files');
  };

  return (
    <div styleName="container">
      <img src={gopher404} alt="Gopher" />
      <div styleName="link" onClick={clickReturn}>
        return
      </div>
    </div>
  );
};

export default NotFound;
