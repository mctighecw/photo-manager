import React, { Fragment } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Menu, Button, Divider } from 'antd';
import { removeCookie } from 'Utils/cookieFunctions';

import { FileOutlined, SmileOutlined, InfoCircleOutlined } from '@ant-design/icons';
import LogoutIcon from 'Assets/logout.svg';
import './styles.scss';

const TopMenu = (props) => {
  const history = useHistory();
  const location = useLocation();
  const selectedKey = location.pathname.slice(1);

  const handleClickMenu = (e) => {
    const { key } = e;
    const path = `/${key}`;
    history.push(path);
  };

  const handleLogout = () => {
    removeCookie();
    history.push('/login');
  };

  return (
    <Fragment>
      <div styleName="content">
        <Menu onClick={handleClickMenu} selectedKeys={[selectedKey]} mode="horizontal">
          <Menu.Item key="files">
            <FileOutlined />
            Files
          </Menu.Item>
          <Menu.Item key="faces">
            <SmileOutlined />
            Faces
          </Menu.Item>
          <Menu.Item key="about">
            <InfoCircleOutlined />
            About
          </Menu.Item>
        </Menu>

        <Button type="primary" size="middle" onClick={handleLogout}>
          Logout
          <img src={LogoutIcon} alt="" styleName="logout-icon" />
        </Button>
      </div>
      <Divider style={{ marginTop: 10, marginBottom: 0 }} />
    </Fragment>
  );
};

export default TopMenu;
