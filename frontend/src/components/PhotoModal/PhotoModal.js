import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'antd';
import { getUrl } from 'Network/urls';
import { post } from 'Network/requests';
import './styles.scss';

const PhotoModal = (props) => {
  const { photoData, handleClose } = props;
  const { hashedName, originalName, extension } = photoData;
  const [photo, setPhoto] = useState(null);
  const [status, setStatus] = useState(null);
  const fileName = originalName + '.' + extension;

  const handleDownload = () => {
    const url = getUrl('file', 'download');
    const data = { hashed_name: hashedName, extension };

    post(url, data, true)
      .then((res) => {
        const data = `data:image/${extension};base64,${res.data.file}`;
        setPhoto(data);
      })
      .catch((err) => {
        console.log(err);
        setStatus('Photo failed to load');
      });
  };

  useEffect(() => {
    setStatus('Loading photo...');
    handleDownload();
  }, []);

  return (
    <Modal title={fileName} visible={true} footer={null} onCancel={handleClose}>
      {photo === null ? (
        <div styleName="photo-status">{status !== null && status}</div>
      ) : (
        <img src={photo} alt="Photo" styleName="photo" />
      )}

      <Button type="primary" size="middle" onClick={handleClose}>
        Close
      </Button>
    </Modal>
  );
};

export default PhotoModal;
