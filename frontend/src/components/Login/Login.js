import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Input, Button } from 'antd';
import { post } from 'Network/requests';
import { getUrl } from 'Network/urls';
import { setCookie, getCookie } from 'Utils/cookieFunctions';

import './styles.scss';

const Login = (props) => {
  const history = useHistory();
  const [username, setUsername] = useState('jsmith');
  const [password, setPassword] = useState('john1');
  const [error, setError] = useState('');

  const handleChange = (e, func) => {
    func(e.target.value);
  };

  const handleSubmit = () => {
    setError('');

    if (username === '' || password === '') {
      setError('Both fields are required');
    } else {
      handleLogIn();
    }
  };

  const handleLogIn = () => {
    const url = getUrl('auth', 'login');
    const data = {
      username,
      password,
    };

    post(url, data, false)
      .then((res) => {
        if (res.status === 200) {
          const { data } = res;
          const { message, status, token } = data;
          setCookie(token);
          history.push('/files');
        } else {
          setError(`Error (status ${res.status})`);
        }
      })
      .catch((err) => {
        if (err.response && err.response.status === 401) {
          setError('Wrong credentials');
        } else {
          setError(`Error (status ${err.response.status})`);
        }
      });
  };

  useEffect(() => {
    const token = getCookie('token');

    if (token) {
      history.push('/files');
    } else {
      console.log('No valid user cookie');
    }
  }, []);

  return (
    <div styleName="container">
      <div styleName="box">
        <div styleName="heading">Login</div>
        <Input
          size="large"
          placeholder="Username"
          value={username}
          onChange={(e) => handleChange(e, setUsername)}
        />
        <div styleName="spacer" />
        <Input.Password
          size="large"
          placeholder="Password"
          value={password}
          onChange={(e) => handleChange(e, setPassword)}
        />
        <div styleName="error">{error}</div>

        <Button type="primary" size="middle" onClick={handleSubmit}>
          Submit
        </Button>
      </div>
    </div>
  );
};

export default Login;
