import moment from 'moment';

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment.utc(datestring).utcOffset(offset).format();
  return currentDateTime;
};

export const formatDateTime = (datestring) => {
  const currentDateTime = getOffset(datestring);
  const date = moment(currentDateTime).format('HH:mm, DD MMMM YYYY');
  return date;
};

export const cleanFileName = (filename) => {
  // Only allow letters and numbers
  const res = filename.replace(/[^a-zA-Z0-9]/g, '');
  return res;
};

export const cleanHashtag = (tag) => {
  // Only allow letters and numbers; convert to lowercase
  let res = tag.replace(/[^a-zA-Z0-9]/g, '');
  res = res.toLowerCase();
  return res;
};

export const bytesToSize = (bytes) => {
  const sizes = ['Bytes', 'KB', 'MB'];
  let res = '0';

  if (bytes > 0) {
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    res = Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  }
  return res;
};

export const stringToHexColor = (str) => {
  // Generate a hex color code from a string (hashtag)
  let hash = 0;
  let color = '#';

  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }

  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xff;
    color += ('00' + value.toString(16)).substr(-2);
  }
  return color;
};

export const checkNullArray = (value) => {
  const res = value === null ? [] : value;
  return res;
};

export const renderCommaSeparatedItems = (value) => {
  const array = checkNullArray(value);
  let list = '';

  if (array.length > 0) {
    array.forEach((item, index) => {
      if (array.length !== index + 1) {
        list += item + ', ';
      } else {
        list += item;
      }
    });
  } else {
    list = 'None';
  }
  return list;
};
