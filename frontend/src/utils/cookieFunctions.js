export const setCookie = (token) => {
  let d = new Date();
  d.setTime(d.getTime() + 24 * 60 * 60 * 1000);
  d.toUTCString();
  document.cookie = `token=${token};expires=${d};path=/`;
};

export const getCookie = (name) => {
  const value = '; ' + document.cookie;
  const parts = value.split('; ' + name + '=');

  if (parts.length == 2) {
    return parts.pop().split(';').shift();
  }
};

export const removeCookie = () => {
  document.cookie = 'token=;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/';
};
