# README

An app to manage photos. The user can upload pictures, organize them, edit their information, and have the faces recognized based on uploaded sample face files. The photos are saved to an AWS S3 bucket and the photo/facial information saved to a database.

React frontend and Go (_Golang_) backend, using the framework _Echo_, with a REST API connected to MongoDB.

## App Information

App Name: photo-manager

Created: April-June 2020

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/photo-manager)

Production: [Link](https://photo-manager.mctighecw.site)

## Tech Stack

- React
- Ant Design (Less)
- Sass & CSS Modules
- Go
- Echo
- JWT Authentication
- [Go-Face](https://github.com/Kagami/go-face)
- MongoDB
- Docker

## Run with Docker

### Development

The Go CompileDaemon rebuilds and restarts after each change of code.
Follow the backend logs to see the real-time log output.

```
$ docker-compose -f docker-compose-dev.yml up -d --build
$ docker logs -f photo-manager_backend_1
```

### Production

```
$ docker-compose -f docker-compose-prod.yml up -d --build
```

### Set Up Database (for both environments)

```
$ docker exec -it photo-manager_mongodb_1 bash
  (set up admin user and password)
$ source init_docker_db.sh
```

## Credits

I used this tutorial to get started with facial recognition in Go: [Go Face Recognition Tutorial](https://hackernoon.com/go-face-recognition-tutorial-part-1-373357230baa).

Icons:

- Loading spinner created at [Loading.io](https://loading.io)
- Picture icon (_favicon_) courtesy of [Freepik](https://www.flaticon.com/authors/freepik)
- Logout icon courtesy of [Google](https://www.flaticon.com/authors/google)
- Upload icon coursesy of [Kiranshastry](https://www.flaticon.com/authors/kiranshastry)
- Go gopher courtesy of [Renee French](http://reneefrench.blogspot.com)

Last updated: 2025-01-10
