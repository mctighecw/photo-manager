package main

import (
  "../conf"
  "../db"
  "../lib"

  "go.mongodb.org/mongo-driver/bson"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollUsers = conf.DB_CONFIG.CollUsers
  CollFiles = conf.DB_CONFIG.CollFiles
  CollFaces = conf.DB_CONFIG.CollFaces
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll1 := db.Collection(CollUsers)
  coll2 := db.Collection(CollFiles)
  coll3 := db.Collection(CollFaces)

  f := bson.M{}
  _, err1 := coll1.DeleteMany(ctx, f)
  _, err2 := coll2.DeleteMany(ctx, f)
  _, err3 := coll3.DeleteMany(ctx, f)

  if err1 != nil || err2 != nil || err3 != nil {
    lib.PrintMessage("Error dropping database " + DbName)
  } else {
    lib.PrintMessage("OK, database has been dropped")
  }
}
