package main

import (
  "../conf"
  "../db"
  "../lib"

  "fmt"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollUsers = conf.DB_CONFIG.CollUsers

  seeds = []interface{}{}
  u1 = db.U1
  u2 = db.U2
  u3 = db.U3
)

func main() {
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  seeds = append(seeds, u1, u2, u3)
  result, err := coll.InsertMany(ctx, seeds)

  lib.LogError(err)
  fmt.Println(result)
}
