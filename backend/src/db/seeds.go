package db

import (
  "../lib"
  "../models"

  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  U1 = models.User{
    ID:         primitive.NewObjectID(),
    Username:  "jsmith",
    Name:      "John Smith",
    Admin:     false,
    Password:  lib.HashPassword("john1"),
    CreatedAt: time.Now(),
  }

  U2 = models.User{
    ID:         primitive.NewObjectID(),
    Username:  "swilliams",
    Name:      "Susan Williams",
    Admin:     false,
    Password:  lib.HashPassword("susan1"),
    CreatedAt: time.Now(),
  }

  U3 = models.User{
    ID:         primitive.NewObjectID(),
    Username:  "sjones",
    Name:      "Sam Jones",
    Admin:     false,
    Password:  lib.HashPassword("sam1"),
    CreatedAt: time.Now(),
  }
)
