package operations

import (
  "../conf"
  "../lib"

  "os"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func UploadFileToS3(path string, fullHashedName string) bool {
  AWS_BUCKET_NAME := conf.AWS_BUCKET_NAME

  sess := createSession()

  file, err1  := os.Open(path)
  lib.LogError(err1)
  defer file.Close()

  uploader := s3manager.NewUploader(sess)

  _, err2 := uploader.Upload(&s3manager.UploadInput{
    Bucket: aws.String(AWS_BUCKET_NAME),
    Key: aws.String(fullHashedName),
    Body: file,
  })

  var success bool

  if err2 != nil {
    lib.LogError(err2)
    success = false
  } else {
    success = true
    os.Remove(path)
  }
  return success
}
