package operations

import (
  "../conf"
  "../lib"

  "fmt"
  "os"
  "path/filepath"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3"
  "github.com/aws/aws-sdk-go/service/s3/s3manager"
)

func DownloadFileFromS3(hashedName string, extension string) (bool, string, string) {
  AWS_BUCKET_NAME := conf.AWS_BUCKET_NAME
  fullHashedName := hashedName + "." + extension

  sess := createSession()

  rootDir := lib.GetRootDir()
  path := filepath.Join(rootDir, "temp", fullHashedName)

  file, err1 := os.Create(path)
  if err1 != nil {
    lib.LogError(err1)
  }
  defer file.Close()

  downloader := s3manager.NewDownloader(sess)

  numBytes, err2 := downloader.Download(file,
    &s3.GetObjectInput{
      Bucket: aws.String(AWS_BUCKET_NAME),
      Key: aws.String(fullHashedName),
    },
  )

  var success bool
  var status string

  if err2 != nil {
    os.Remove(path)
    lib.LogError(err2)
    success = false
    status = fmt.Sprintf("Error downloading file %s", fullHashedName)
  } else {
    success = true
    status = fmt.Sprintf("Downloaded file %s (%d bytes)", fullHashedName, numBytes)
  }

  return success, status, path
}
