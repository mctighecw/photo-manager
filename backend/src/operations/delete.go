package operations

import (
  "../conf"
  "../lib"

  "fmt"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/service/s3"
)

func DeleteFileFromS3(fileName string, extension string) (bool, string) {
  AWS_BUCKET_NAME := conf.AWS_BUCKET_NAME
  fullFileName := fileName + "." + extension

  sess := CreateS3Session()

  input := &s3.DeleteObjectInput{
    Bucket: aws.String(AWS_BUCKET_NAME),
    Key: aws.String(fullFileName),
  }

  _, err := sess.DeleteObject(input)

  var success bool
  var status string

  if err != nil {
    lib.LogError(err)
    success = false
    status = fmt.Sprintf("Error deleting file %s", fullFileName)
  } else {
    success = true
    status = fmt.Sprintf("File %s deleted successfully", fullFileName)
  }
  return success, status
}
