package operations

import (
  "../conf"
  "../lib"

  "github.com/aws/aws-sdk-go/aws"
  "github.com/aws/aws-sdk-go/aws/credentials"
  "github.com/aws/aws-sdk-go/aws/session"
  "github.com/aws/aws-sdk-go/service/s3"
)

func createSession() (*session.Session) {
  AWS_REGION_NAME := conf.AWS_REGION_NAME
  AWS_SECRET_ID := conf.AWS_SECRET_ID
  AWS_SECRET_KEY := conf.AWS_SECRET_KEY

  sess, err := session.NewSession(&aws.Config{
      Region: aws.String(AWS_REGION_NAME),
      Credentials: credentials.NewStaticCredentials(
        AWS_SECRET_ID,
        AWS_SECRET_KEY,
        "",
      ),
    },
  )

  lib.LogError(err)

  return sess
}

func CreateS3Session() *s3.S3 {
  AWS_REGION_NAME := conf.AWS_REGION_NAME
  AWS_SECRET_ID := conf.AWS_SECRET_ID
  AWS_SECRET_KEY := conf.AWS_SECRET_KEY

  sess := s3.New(session.New(), &aws.Config{
    Region: aws.String(AWS_REGION_NAME),
    Credentials: credentials.NewStaticCredentials(
      AWS_SECRET_ID,
      AWS_SECRET_KEY,
      "",
    ),
  })

  return sess
}
