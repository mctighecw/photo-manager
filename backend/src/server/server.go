package server

import (
  "../conf"
  "../handlers"
  "../lib"

  "github.com/labstack/echo"
  "github.com/labstack/echo/middleware"
)

func CreateServer() *echo.Echo {
  e := echo.New()

  e.Use(middleware.Recover())

  e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
    Format: "${time_rfc3339_nano}  ${method}  ${uri}  ${status}  ${error}\n",
  }))

  e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
    AllowOrigins: []string{"*"},
    AllowMethods: []string{echo.HEAD, echo.GET, echo.POST},
  }))

  a := e.Group("/api")
  t := a.Group("/auth")
  t.POST("/signup", handlers.SignUp)
  t.POST("/login", handlers.Login)

  // user routes
  u := a.Group("/user")
  u.Use(middleware.JWTWithConfig(conf.JWT_CONFIG))
  u.GET("/self", handlers.UserInfo)
  u.GET("/all", handlers.UsersInfo)

  // file routes
  f := a.Group("/file")
  f.Use(middleware.JWTWithConfig(conf.JWT_CONFIG))
  f.Use(middleware.BodyLimit("2M"))
  f.GET("/all", handlers.GetUserFiles)
  f.POST("/upload", handlers.UploadFile)
  f.POST("/download", handlers.DownloadFile)
  f.POST("/info", handlers.UpdateFileInfo)
  f.POST("/faces", handlers.UpdateFileFaces)
  f.POST("/delete", handlers.DeleteFile)

  // faces routes
  g := a.Group("/faces")
  g.Use(middleware.JWTWithConfig(conf.JWT_CONFIG))
  g.Use(middleware.BodyLimit("1M"))
  g.GET("/all", handlers.GetUserFaces)
  g.POST("/upload", handlers.UploadFaceFile)
  g.POST("/download", handlers.DownloadToServer)
  g.POST("/remove", handlers.RemoveFromServer)
  g.POST("/update", handlers.UpdateFace)
  g.POST("/delete", handlers.DeleteFace)
  g.POST("/count", handlers.CountFaces)
  g.POST("/clear", handlers.ClearFaces)
  g.POST("/identify", handlers.IdentifyFaces)

  m := lib.GetEnv("APP_ENV", "development")
  mode := "Mode: " + m
  lib.PrintMessage(mode)

  return e
}
