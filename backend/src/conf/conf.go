package conf

import (
  "../lib"

  "log"
  "os"

  "github.com/joho/godotenv"
)

type (
  App struct {
    Name string
    Port uint
  }

  Db struct {
    Host string
    Port uint
    User string
    Pw string
    AdminDb string
    DbName string
    CollUsers string
    CollFiles string
    CollFaces string
  }
)

var (
  APP_CONFIG *App
  DB_CONFIG *Db

  APP_ENV string

  DB_HOST string
  DB_USER string
  DB_PW string
  DB_ADMIN string
  DB_NAME string
  COLL_USERS string
  COLL_FILES string
  COLL_FACES string

  AWS_REGION_NAME string
  AWS_SECRET_ID string
  AWS_SECRET_KEY string
  AWS_BUCKET_NAME string
)

func init() {
  APP_ENV = lib.GetEnv("APP_ENV", "development")

  if APP_ENV == "development" {
    err := godotenv.Load()
    if err != nil {
      log.Fatal("Error loading .env file")
    }
  }

  initConfig()
}

func initConfig() {
  APP_CONFIG = &App {
    Name: "Photo Manager Server",
    Port: 1323,
  }

  if APP_ENV == "development" {
    DB_HOST = os.Getenv("DB_HOST_DEV")
  } else {
    DB_HOST = os.Getenv("DB_HOST_PROD")
  }

  DB_USER = os.Getenv("DB_USER")
  DB_PW = os.Getenv("DB_PW")
  DB_ADMIN = os.Getenv("DB_ADMIN")
  DB_NAME = os.Getenv("DB_NAME")
  COLL_USERS = os.Getenv("COLL_USERS")
  COLL_FILES = os.Getenv("COLL_FILES")
  COLL_FACES = os.Getenv("COLL_FACES")

  AWS_REGION_NAME = os.Getenv("AWS_REGION_NAME")
  AWS_SECRET_ID = os.Getenv("AWS_SECRET_ID")
  AWS_SECRET_KEY = os.Getenv("AWS_SECRET_KEY")
  AWS_BUCKET_NAME = os.Getenv("AWS_BUCKET_NAME")

  DB_CONFIG = &Db {
    Host: DB_HOST,
    Port: 27017,
    User: DB_USER,
    Pw: DB_PW,
    AdminDb: DB_ADMIN,
    DbName: DB_NAME,
    CollUsers: COLL_USERS,
    CollFiles: COLL_FILES,
    CollFaces: COLL_FACES,
  }
}
