package conf

import (
  "os"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo/middleware"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  JWT_CONFIG middleware.JWTConfig
  JWT_SECRET string
)

type JwtCustomClaims struct {
  ID primitive.ObjectID `json:"id"`
  Username string `json:"username"`
  Name string `json:"name"`
  Admin bool `json:"admin"`
  jwt.StandardClaims
}

func init() {
  JWT_SECRET = os.Getenv("JWT_SECRET")

  JWT_CONFIG = middleware.JWTConfig{
    Claims: &JwtCustomClaims{},
    SigningKey: []byte(JWT_SECRET),
  }
}
