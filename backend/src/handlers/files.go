package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"
  "../operations"

  "net/http"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func GetUserFiles(c echo.Context) error {
  // GET - returns all photo files information for a user
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)
  userId := claims.ID.Hex()

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFiles)

  // Find all user files that are not deleted
  cursor, err := coll.Find(ctx, bson.M{"user_id": userId, "deleted": false})
  lib.LogError(err)

  res := []models.File{}

  for cursor.Next(ctx) {
    f := models.File{}
    cursor.Decode(&f)
    res = append(res, f)
  }

  d := lib.ReturnAllFilesData(res)
  return c.JSON(http.StatusOK, d)
}

func DeleteFile(c echo.Context) error {
  // POST - marks a photo file as deleted in database
  // and removes file from AWS
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFiles)

  // Update database
  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "deleted": true,
        "deleted_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount

  // Delete file on AWS
  findRes := coll.FindOne(ctx, bson.M{"_id": objID})
  err3 := findRes.Err()

  file := models.File{}
  findRes.Decode(&file)

  success, status := operations.DeleteFileFromS3(file.HashedName, file.Extension)
  var res interface{}

  if err1 != nil || err2 != nil || err3 != nil || modCount == 0 || success == false {
    res = lib.ReturnError(status)
  } else {
    res = lib.ReturnOK(status)
  }

  return c.JSON(http.StatusOK, res)
}

func UpdateFileInfo(c echo.Context) error {
  // POST - updates a photo file's info: name, category, hashtags
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  original_name := lib.ReturnStringValue(m, "original_name")
  category := lib.ReturnStringValue(m, "category")
  hashtags := lib.ReturnSliceValue(m, "hashtags")

  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFiles)

  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "original_name": original_name,
        "category": category,
        "hashtags": hashtags,
        "updated_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount

  var status string
  var res interface{}

  if err1 != nil || err2 != nil || modCount == 0 {
    status = "Error updating file"
    res = lib.ReturnError(status)
  } else {
    status = "File has been updated"
    res = lib.ReturnOK(status)
  }

  return c.JSON(http.StatusOK, res)
}

func UpdateFileFaces(c echo.Context) error {
  // POST - updates a photo file's faces database field
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  faces := lib.ReturnSliceValue(m, "faces")

  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFiles)

  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "faces": faces,
        "updated_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount
  var res interface{}

  if err1 != nil || err2 != nil || modCount == 0 {
    res = lib.ReturnError("Error updating faces")
  } else {
    res = lib.ReturnOK("Faces have been updated")
  }

  return c.JSON(http.StatusOK, res)
}
