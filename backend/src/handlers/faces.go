package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"
  "../operations"
  "../recognizer"

  "net/http"
  "os"
  "path/filepath"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/Kagami/go-face"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  DbName = conf.DB_CONFIG.DbName
  CollFiles = conf.DB_CONFIG.CollFiles
  CollFaces = conf.DB_CONFIG.CollFaces
)

func CountFaces(c echo.Context) error {
  // POST - returns number of faces in photo file
  m := lib.GetEchoMap(c)

  filename := lib.ReturnStringValue(m, "filename")

  rootDir := lib.GetRootDir()
  path := filepath.Join(rootDir, "temp")

  filenameShort, extension := lib.GetFileNameExtension(filename)

  // Convert file type if not jpg
  if extension != "jpg" {
    lib.ConvertToJpg(filename, path)
  }

  newFilename := filenameShort + ".jpg"
  count := recognizer.CountFaces(newFilename, path)

  // Remove converted jpg file
  if extension != "jpg" {
    newFilePath := filepath.Join(path, newFilename)
    os.Remove(newFilePath)
  }

  message := "Face count was successful"
  res := lib.ReturnFaceCount(message, count)
  return c.JSON(http.StatusOK, res)
}

func GetUserFaces(c echo.Context) error {
  // GET - returns all faces file information for a user
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)
  userId := claims.ID.Hex()

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFaces)

  // Find all user faces that are not deleted
  cursor, err := coll.Find(ctx, bson.M{"user_id": userId, "deleted": false})
  lib.LogError(err)

  res := []models.Face{}

  for cursor.Next(ctx) {
    f := models.Face{}
    cursor.Decode(&f)
    res = append(res, f)
  }

  d := lib.ReturnAllFacesData(res)
  return c.JSON(http.StatusOK, d)
}

func UpdateFace(c echo.Context) error {
  // POST - updates name of identified face
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  name := lib.ReturnStringValue(m, "name")

  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFaces)

  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "name": name,
        "updated_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount

  var status string
  var res interface{}

  if err1 != nil || err2 != nil || modCount == 0 {
    status = "Error updating face name"
    res = lib.ReturnError(status)
  } else {
    status = "Face name has been updated"
    res = lib.ReturnOK(status)
  }

  return c.JSON(http.StatusOK, res)
}

func DeleteFace(c echo.Context) error {
  // POST - marks an identified face as deleted in database
  // and removes file from AWS
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFaces)

  // Update database
  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "deleted": true,
        "deleted_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount

  // Delete file on AWS
  findRes := coll.FindOne(ctx, bson.M{"_id": objID})
  err3 := findRes.Err()

  file := models.Face{}
  findRes.Decode(&file)

  success, status := operations.DeleteFileFromS3(file.HashedName, file.Extension)

  var res interface{}

  if err1 != nil || err2 != nil || err3 != nil ||  success == false || modCount == 0 {
    res = lib.ReturnError(status)
  } else {
    res = lib.ReturnOK(status)
  }

  return c.JSON(http.StatusOK, res)
}

func ClearFaces(c echo.Context) error {
  // POST - clears a photo file's database faces field
  m := lib.GetEchoMap(c)

  id := lib.ReturnStringValue(m, "id")
  objID, err1 := primitive.ObjectIDFromHex(id)

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollFiles)

  updateRes, err2 := coll.UpdateOne(
    ctx,
    bson.M{"_id": objID},
    bson.M{
      "$set": bson.M{
        "faces": []string{},
        "updated_at": time.Now(),
      },
    },
  )

  modCount := updateRes.ModifiedCount
  var res interface{}

  if err1 != nil || err2 != nil || modCount == 0 {
    res = lib.ReturnError("Error clearing faces")
  } else {
    res =  lib.ReturnOK("Faces have been cleared")
  }

  return c.JSON(http.StatusOK, res)
}


func IdentifyFaces(c echo.Context) error {
  // POST - identifies all faces found in a photo file and
  // saves names to database
  m := lib.GetEchoMap(c)

  // Filename and path
  filename := lib.ReturnStringValue(m, "filename")
  rootDir := lib.GetRootDir()
  path := filepath.Join(rootDir, "temp")

  // User info
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)
  userId := claims.ID.Hex()

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll1 := db.Collection(CollFaces)

  // Find all user faces that are not deleted
  cursor, err := coll1.Find(ctx, bson.M{"user_id": userId, "deleted": false})
  lib.LogError(err)

  // Prepare for classification
  var descriptors []face.Descriptor
  var names []string
  var positions []int32
  var i int32 = 0

  for cursor.Next(ctx) {
    f := models.Face{}
    cursor.Decode(&f)

    descriptors = append(descriptors, f.Descriptor)
    names = append(names, f.Name)
    positions = append(positions, int32(i))
    i += 1
  }

  filenameShort, extension := lib.GetFileNameExtension(filename)

  // Convert file type if not jpg
  if extension != "jpg" {
    lib.ConvertToJpg(filename, path)
  }

  // New file name and path
  newFilename := filenameShort + ".jpg"
  newFilePath := filepath.Join(path, newFilename)

  success, facesSlice := recognizer.IdentifyFacesInPhoto(newFilePath, names, positions, descriptors)
  var res interface{}

  if (success) {
    // Save identified faces to database
    id := lib.ReturnStringValue(m, "id")
    objID, err1 := primitive.ObjectIDFromHex(id)
    coll2 := db.Collection(CollFiles)

    updateRes, err2 := coll2.UpdateOne(
      ctx,
      bson.M{"_id": objID},
      bson.M{
        "$set": bson.M{
          "faces": facesSlice,
          "updated_at": time.Now(),
        },
      },
    )

    modCount := updateRes.ModifiedCount

    if err1 != nil || err2 != nil || modCount == 0 {
      res = lib.ReturnError("Facial identification succeeded but failed to save to database")
    } else {
      m := "Facial identification success"
      c := len(facesSlice)
      res = lib.ReturnFaceCount(m, c)
    }
  } else {
    res = lib.ReturnError("Facial identification failed")
  }

  // Remove converted jpg file
  if extension != "jpg" {
    os.Remove(newFilePath)
  }

  return c.JSON(http.StatusOK, res)
}
