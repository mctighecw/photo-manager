package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"
  "../operations"
  "../recognizer"

  "io"
  "io/ioutil"
  "net/http"
  "os"
  "path/filepath"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func UploadFaceFile(c echo.Context) error {
  // POST - identify face in photo and save to database,
  // then upload file to AWS
  name := c.FormValue("name")

  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)
  userId := claims.ID.Hex()

  file, err := c.FormFile("file")
  lib.LogError(err)

  // Open file
  src, err := file.Open()
  lib.LogError(err)
  defer src.Close()

  // Destination to save
  root := lib.GetRootDir()
  path := filepath.Join(root, "temp")
  originalPath := filepath.Join(path, file.Filename)
  dst, err := os.Create(originalPath)
  lib.LogError(err)
  defer dst.Close()

  // Copy data
  _, err = io.Copy(dst, src)
  lib.LogError(err)

  filename, extension := lib.GetFileNameExtension(file.Filename)

  // Convert file type if not jpg
  if extension != "jpg" {
    lib.ConvertToJpg(file.Filename, path)
  }

  // File name management
  hashedName := lib.MakeRandomString(16)
  fullHashedName := hashedName + "." + extension

  // File size
  fileSizeInt := lib.ConvertInt64ToInt(file.Size)

  // Check face count
  newFilename := filename + ".jpg"
  count := recognizer.CountFaces(newFilename, path)

  var res interface{}

  if count == 1 {
    // Face descriptor
    newFilePath := filepath.Join(path, newFilename)
    descriptor := recognizer.CreateFaceDescriptor(newFilePath)

    // Upload to AWS
    ok := operations.UploadFileToS3(originalPath, fullHashedName)
    if ok {
      // Add new file info to database
      client, ctx := db.DbConnect()
      db := client.Database(DbName)
      coll := db.Collection(CollFaces)

      f := models.Face{
        ID: primitive.NewObjectID(),
        UserId: userId,
        Name: name,
        HashedName: hashedName,
        Extension: extension,
        Deleted: false,
        Size: fileSizeInt,
        Descriptor: descriptor,
        CreatedAt: time.Now(),
      }

      _, err := coll.InsertOne(ctx, f)
      lib.LogError(err)

      // Remove temporary file
      os.Remove(newFilePath)
    }

    if ok {
      m := "Face identified and file uploaded successfully"
      res = lib.ReturnOK(m)
    } else {
      m := "Face identified but error uploading face photo"
      res = lib.ReturnError(m)
    }
  } else if count == 0 {
    m := "No faces were found in photo"
    res = lib.ReturnError(m)
  } else {
    m := "More than one face was found in photo"
    res = lib.ReturnError(m)
  }

  // Remove temporary file
  os.Remove(originalPath)

  return c.JSON(http.StatusOK, res)
}

func DownloadToServer(c echo.Context) error {
  // POST - downloads file from AWS to server for
  // recognizer operation
  m := lib.GetEchoMap(c)

  hashedName := lib.ReturnStringValue(m, "hashed_name")
  extension := lib.ReturnStringValue(m, "extension")

  success, _, path := operations.DownloadFileFromS3(hashedName, extension)

  file, err1 := os.Open(path)
  lib.LogError(err1)
  defer file.Close()

  _, err2 := ioutil.ReadAll(file)
  lib.LogError(err2)

  var res interface{}

  if err1 != nil || err2 != nil || !success {
    res = lib.ReturnError("Error downloading file to server")
  } else {
    res = lib.ReturnOK("File downloaded to server")
  }

  return c.JSON(http.StatusOK, res)
}

func RemoveFromServer(c echo.Context) error {
  // POST - removes file downloaded to server after
  // recognizer operation
  m := lib.GetEchoMap(c)

  hashedName := lib.ReturnStringValue(m, "hashed_name")
  extension := lib.ReturnStringValue(m, "extension")
  fullHashedName := hashedName + "." + extension

  rootDir := lib.GetRootDir()
  path := filepath.Join(rootDir, "temp", fullHashedName)

  os.Remove(path)

  res := lib.ReturnOK("File removed from server")
  return c.JSON(http.StatusOK, res)
}
