package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"

  "net/http"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

var (
  CollUsers = conf.DB_CONFIG.CollUsers
)

func SignUp(c echo.Context) error {
  // POST - creates a new user with a unique username
  // and saves info to database
  m := lib.GetEchoMap(c)

  _, field1 := m["username"]
  _, field2 := m["name"]
  _, field3 := m["password"]

  var res interface{}

  if field1 && field2 && field3 {
    username := lib.ReturnStringValue(m, "username")

    client, ctx := db.DbConnect()
    db := client.Database(DbName)
    coll := db.Collection(CollUsers)

    findRes := coll.FindOne(ctx, bson.M{"username": username})
    err := findRes.Err()

    if err != nil {
      name := lib.ReturnStringValue(m, "name")
      password := lib.ReturnStringValue(m, "password")
      hashedPassword := lib.HashPassword(password)

      u := models.User{
        ID: primitive.NewObjectID(),
        Username: username,
        Name: name,
        Admin: false,
        Password: hashedPassword,
        CreatedAt: time.Now(),
      }

      _, err := coll.InsertOne(ctx, u)
      lib.LogError(err)

      if err != nil {
        m := "Error saving new user data"
        res = lib.ReturnError(m)
      } else {
        m := "New user created successfully"
        res = lib.ReturnOK(m)
      }
    } else {
      m := "Error creating new user - username already exists"
      res = lib.ReturnError(m)
    }
  } else {
    m := "Error creating new user - missing fields"
    res = lib.ReturnError(m)
  }

  return c.JSON(http.StatusOK, res)
}

func Login(c echo.Context) error {
  // POST - logs a user in with a username and password
  m := lib.GetEchoMap(c)

  username := lib.ReturnStringValue(m, "username")
  password := lib.ReturnStringValue(m, "password")

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  res := coll.FindOne(ctx, bson.M{"username": username})
  err := res.Err()

  user := models.User{}
  res.Decode(&user)
  pwValid := lib.CheckPasswordHash(password, user.Password)

  if err != nil || !pwValid {
    return echo.ErrUnauthorized
  }

  id := user.ID
  name := user.Name
  admin := user.Admin
  expiration := time.Now().Add(time.Hour * 72)

  claims := &conf.JwtCustomClaims{
    id,
    username,
    name,
    admin,
    jwt.StandardClaims{
      ExpiresAt: expiration.Unix(),
    },
  }

  token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

  t, err := token.SignedString([]byte(conf.JWT_SECRET))
  if err != nil {
    return err
  }

  message := username + " has successfully logged in"
  r := lib.ReturnTokenOK(message, t)

  return c.JSON(http.StatusOK, r)
}

func UserInfo(c echo.Context) error {
  // GET - returns the user info from the jwt claims
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)

  r := map[string]interface{}{
    "status": "User logged in",
    "username": claims.Username,
    "name": claims.Name,
    "admin": claims.Admin,
  }

  return c.JSON(http.StatusOK, r)
}

func UsersInfo(c echo.Context) error {
  // GET - returns information for all users from database
  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)

  // Only admin users are authorized
  if (!claims.Admin) {
    return echo.ErrUnauthorized
  }

  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll := db.Collection(CollUsers)

  u := models.User{}
  res := []models.User{}
  cursor, err := coll.Find(ctx, bson.M{})
  lib.LogError(err)

  for cursor.Next(ctx) {
    cursor.Decode(&u)
    res = append(res, u)
  }

  d := lib.ReturnAllUsersData(res)
  return c.JSON(http.StatusOK, d)
}
