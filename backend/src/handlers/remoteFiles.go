package handlers

import (
  "../conf"
  "../db"
  "../lib"
  "../models"
  "../operations"
  "../recognizer"

  "fmt"
  "io"
  "io/ioutil"
  "mime/multipart"
  "net/http"
  "os"
  "path/filepath"
  "time"

  "github.com/dgrijalva/jwt-go"
  "github.com/Kagami/go-face"
  "github.com/labstack/echo"
  "go.mongodb.org/mongo-driver/bson"
  "go.mongodb.org/mongo-driver/bson/primitive"
)

func uploadSaveFile(file *multipart.FileHeader, userId string) bool {
  // Function to identify faces in a photo file,
  // upload to AWS, and save info in database

  // Check file name length and extension
  filenameRaw, extension := lib.GetFileNameExtension(file.Filename)
  filenameShort := lib.TruncateString(filenameRaw, 20)

  if extension == "jpeg" {
    extension = "jpg"
  }

  filename := filenameShort + "." + extension

  // Open file
  src, err := file.Open()
  lib.LogError(err)
  defer src.Close()

  // Destination to save
  rootDir := lib.GetRootDir()
  path := filepath.Join(rootDir, "temp")
  originalPath := filepath.Join(path, filename)
  dst, err := os.Create(originalPath)
  lib.LogError(err)
  defer dst.Close()

  // Copy data
  _, err = io.Copy(dst, src)
  lib.LogError(err)

  // Convert file type if not jpg
  if extension != "jpg" {
    lib.ConvertToJpg(filename, path)
  }

  // File name management
  hashedName := lib.MakeRandomString(16)
  fullHashedName := hashedName + "." + extension

  // File size
  fileSizeInt := lib.ConvertInt64ToInt(file.Size)

  // Facial recognition
  client, ctx := db.DbConnect()
  db := client.Database(DbName)
  coll1 := db.Collection(CollFaces)

  // Find all user faces that are not deleted
  cursor, err := coll1.Find(ctx, bson.M{"user_id": userId, "deleted": false})
  lib.LogError(err)

  // Prepare for classification
  var descriptors []face.Descriptor
  var names []string
  var positions []int32
  var i int32 = 0

  for cursor.Next(ctx) {
    f := models.Face{}
    cursor.Decode(&f)

    descriptors = append(descriptors, f.Descriptor)
    names = append(names, f.Name)
    positions = append(positions, int32(i))
    i += 1
  }

  // New file name and path
  newFilename := filenameShort + ".jpg"
  newFilePath := filepath.Join(path, newFilename)

  _, facesSlice := recognizer.IdentifyFacesInPhoto(newFilePath, names, positions, descriptors)

  var success bool

  // Upload to AWS
  ok := operations.UploadFileToS3(originalPath, fullHashedName)
  if ok {
    // Add new file info to database
    coll2 := db.Collection(CollFiles)

    f := models.File{
      ID: primitive.NewObjectID(),
      UserId: userId,
      OriginalName: filenameShort,
      HashedName: hashedName,
      Extension: extension,
      Category: "",
      Hashtags: []string{},
      Faces: facesSlice,
      Deleted: false,
      Size: fileSizeInt,
      CreatedAt: time.Now(),
    }

    _, err := coll2.InsertOne(ctx, f)
    lib.LogError(err)

    if err != nil {
      success = false
    } else {
      success = true
    }
  } else {
    success = false
  }

  // Remove temporary file(s)
  os.Remove(newFilePath)

  if extension != "jpg" {
    os.Remove(originalPath)
  }

  return success
}

func UploadFile(c echo.Context) error {
  // POST - uploads and saves info for one or more
  // photo files using the uploadSaveFile() function
  length := c.FormValue("length")

  user := c.Get("user").(*jwt.Token)
  claims := user.Claims.(*conf.JwtCustomClaims)
  userId := claims.ID.Hex()

  var res interface{}

  if length == "1" {
    file, err := c.FormFile("file")
    lib.LogError(err)

    success := uploadSaveFile(file, userId)

    if success {
      m := "1 file uploaded successfully"
      res = lib.ReturnOK(m)
    } else {
      m := "Error uploading file"
      res = lib.ReturnError(m)
    }
  } else {
    filesSuccess := true
    filesLength := lib.ConvertStringToInt(length)

    for i := 0; i < filesLength; i++ {
      fileIndex := fmt.Sprintf("file%d", i)

      file, err := c.FormFile(fileIndex)
      lib.LogError(err)
      success := uploadSaveFile(file, userId)

      if !success {
        filesSuccess = false
      }
    }

    if filesSuccess {
      m := length + " files uploaded successfully"
      res = lib.ReturnOK(m)
    } else {
      m := "Error uploading files"
      res = lib.ReturnError(m)
    }
  }

  return c.JSON(http.StatusOK, res)
}

func DownloadFile(c echo.Context) error {
  // POST - downloads file from AWS and returns raw file data
  // in JSON response to client
  m := lib.GetEchoMap(c)

  hashedName := lib.ReturnStringValue(m, "hashed_name")
  extension := lib.ReturnStringValue(m, "extension")

  success, status, path := operations.DownloadFileFromS3(hashedName, extension)

  file, err := os.Open(path)
  lib.LogError(err)
  defer file.Close()

  fileData, err := ioutil.ReadAll(file)
  lib.LogError(err)

  var res interface{}

  if success {
    res = map[string]interface{}{"status": "OK", "message": status, "file": fileData}
  } else {
    res = lib.ReturnError(status)
  }

  // Remove temporary file
  os.Remove(path)

  return c.JSON(http.StatusOK, res)
}
