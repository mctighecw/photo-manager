package main

import (
  "./conf"
  "./server"

  "fmt"
)

func main() {
  s := server.CreateServer()
  port := fmt.Sprintf(":%d", conf.APP_CONFIG.Port)

  s.Logger.Fatal(s.Start(port))
}
