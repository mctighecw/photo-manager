package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type Face struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  UserId string `bson:"user_id" json:"user_id"`
  Name string `bson:"name" json:"name"`
  HashedName string `bson:"hashed_name" json:"hashed_name"`
  Extension string `bson:"extension" json:"extension"`
  Descriptor [128]float32 `bson:"descriptor" json:"descriptor"`
  Deleted bool `bson:"deleted" json:"deleted"`
  Size int `bson:"size" json:"size"`
  CreatedAt time.Time `bson:"created_at" json:"created_at,omitempty"`
  UpdatedAt time.Time `bson:"updated_at" json:"updated_at,omitempty"`
  DeletedAt time.Time `bson:"deleted_at" json:"deleted_at,omitempty"`
}
