package models

import (
  "time"

  "go.mongodb.org/mongo-driver/bson/primitive"
)

type File struct {
  ID primitive.ObjectID `bson:"_id" json:"id,omitempty"`
  UserId string `bson:"user_id" json:"user_id"`
  OriginalName string `bson:"original_name" json:"original_name"`
  HashedName string `bson:"hashed_name" json:"hashed_name"`
  Extension string `bson:"extension" json:"extension"`
  Category string `bson:"category" json:"category"`
  Hashtags []string `bson:"hashtags" json:"hashtags"`
  Faces []string `bson:"faces" json:"faces"`
  Deleted bool `bson:"deleted" json:"deleted"`
  Size int `bson:"size" json:"size"`
  CreatedAt time.Time `bson:"created_at" json:"created_at,omitempty"`
  UpdatedAt time.Time `bson:"updated_at" json:"updated_at,omitempty"`
  DeletedAt time.Time `bson:"deleted_at" json:"deleted_at,omitempty"`
}
