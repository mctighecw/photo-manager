package lib

import (
  "../models"
)

func ReturnTokenOK(m string, t string) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m, "token": t}
  return r
}

func ReturnError(m string) interface{} {
  r := map[string]interface{}{"status": "Error", "message": m}
  return r
}

func ReturnOK(m string) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m}
  return r
}

func ReturnAllUsersData(d []models.User) interface{} {
  e := []interface{}{}

  for _, el := range d {
    f := map[string]interface{}{}

    f["id"] = el.ID
    f["username"] = el.Username
    f["name"] = el.Name
    f["admin"] = el.Admin
    f["created_at"] = el.CreatedAt

    e = append(e, f)
  }

  r := map[string]interface{}{"status": "OK", "data": e}
  return r
}

func ReturnAllFilesData(d []models.File) interface{} {
  e := []interface{}{}

  for _, el := range d {
    f := map[string]interface{}{}

    f["id"] = el.ID
    f["original_name"] = el.OriginalName
    f["hashed_name"] = el.HashedName
    f["extension"] = el.Extension
    f["category"] = el.Category
    f["hashtags"] = el.Hashtags
    f["faces"] = el.Faces
    f["size"] = el.Size
    f["created_at"] = el.CreatedAt

    e = append(e, f)
  }

  r := map[string]interface{}{"status": "OK", "data": e}
  return r
}

func ReturnAllFacesData(d []models.Face) interface{} {
  e := []interface{}{}

  for _, el := range d {
    f := map[string]interface{}{}

    f["id"] = el.ID
    f["name"] = el.Name
    f["hashed_name"] = el.HashedName
    f["extension"] = el.Extension
    f["size"] = el.Size
    f["created_at"] = el.CreatedAt

    e = append(e, f)
  }

  r := map[string]interface{}{"status": "OK", "data": e}
  return r
}

func ReturnFaceCount(m string, c int) interface{} {
  r := map[string]interface{}{"status": "OK", "message": m, "count": c}
  return r
}
