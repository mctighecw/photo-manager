package lib

import (
  "fmt"
  "image"
  "image/jpeg"
  _ "image/gif"
  _ "image/png"
  "math/rand"
  "os"
  "path/filepath"
  "reflect"
  "strconv"
  "strings"

  "github.com/labstack/echo"
)

const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

func PrintMessage(m string) {
  fmt.Println(m)
}

func LogError(err error) {
  if err != nil {
    fmt.Println(err)
    return
  }
}

func ErrorPanic(err error) {
  if err != nil {
    panic(err)
  }
}

func GetEnv(key, fallback string) string {
  if value, ok := os.LookupEnv(key); ok {
    return value
  }
  return fallback
}

func GetRootDir() string {
  dir, err := os.Getwd()
  LogError(err)
  return dir
}

func GetEchoMap(c echo.Context) echo.Map {
  m := echo.Map{}
  if err := c.Bind(&m); err != nil {
    LogError(err)
  }
  return m
}

func GetFileNameExtension (filename string) (string,string) {
  sp := strings.Split(filename, ".")
  return sp[0], sp[1]
}

func MakeRandomString(n int) string {
  b := make([]byte, n)

  for i := range b {
    b[i] = characters[rand.Int63() % int64(len(characters))]
  }
  return string(b)
}

func ConvertStringToInt (numberStr string) (int) {
  numberInt64, err := strconv.ParseInt(numberStr, 10, 64)
  LogError(err)

  numberInt := int(numberInt64)
  return numberInt
}

func ConvertInt64ToInt (numberInt64 int64) (int) {
  numberInt := int(numberInt64)
  return numberInt
}

func ReturnStringValue(m echo.Map, field string) string {
  v := fmt.Sprintf("%v", m[field])
  return v
}

func ReturnBoolValue(m echo.Map, field string) bool {
  i := m[field]
  var j bool = bool(i.(bool))
  return j
}

func ReturnIntValue(m echo.Map, field string) int {
  // Convert a web transmitted int (converted to
  // float64) back to int
  i := m[field]
  var j int = int(i.(float64))
  return j
}

func ReturnSliceValue(m echo.Map, field string) []string {
  // Convert a web transmitted array (converted to
  // interface) back to slice
  var h []string
  j := m[field]
  s := reflect.ValueOf(j)

  for i := 0; i < s.Len(); i++ {
    v := s.Index(i)
    w := fmt.Sprintf("%v",v)
    h = append(h, w)
  }

  return h
}

func ConvertToJpg(fullFilename string, path string) {
  // Converts a gif or png to a new jpg file in same directory
  oldFilepath := filepath.Join(path, fullFilename)
  filename, _ := GetFileNameExtension(fullFilename)

  newFilename := filename + ".jpg"
  newFilepath := filepath.Join(path, newFilename)

  // open old file
  file, err1 := os.Open(oldFilepath)
  LogError(err1)
  defer file.Close()

  // create new file
  outFile, err2 := os.Create(newFilepath)
  LogError(err2)
  defer outFile.Close()

  // decode
  imageData, _, err3 := image.Decode(file)
  LogError(err3)

  // encode as jpg
  err4 := jpeg.Encode(outFile, imageData, nil)
  LogError(err4)
}

func TruncateString(s string, max int) string {
  var numRunes = 0

  for index, _ := range s {
    numRunes++
    if numRunes > max {
      return s[:index]
    }
  }

  return s
}
