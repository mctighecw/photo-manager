package recognizer

import (
  "../lib"

  "fmt"
  "path/filepath"

  "github.com/Kagami/go-face"
)

var (
  ROOT_DIR string
  MODELS_DIR string
)

func init() {
  ROOT_DIR = lib.GetRootDir()
  MODELS_DIR = filepath.Join(ROOT_DIR, "src", "recognizer", "models")
}

func CountFaces(filename string, path string) int {
  rec, err := face.NewRecognizer(MODELS_DIR)
  lib.LogError(err)
  defer rec.Close()

  image := filepath.Join(path, filename)

  faces, err := rec.RecognizeFile(image)
  lib.LogError(err)

  count := len(faces)

  m := fmt.Sprintf("%d face(s) found", count)
  lib.PrintMessage(m)
  return count
}

func CreateFaceDescriptor(fullPath string) face.Descriptor {
  res := face.Descriptor{}

  rec, err := face.NewRecognizer(MODELS_DIR)
  lib.LogError(err)
  defer rec.Close()

  faces, err := rec.RecognizeFile(fullPath)
  lib.LogError(err)

  count := len(faces)

  if count == 1 {
    lib.PrintMessage("The face has been recognized")
    res = faces[0].Descriptor
  }
  return res
}

func IdentifyFacesInPhoto(fullPath string, names []string, positions []int32, descriptors []face.Descriptor) (bool, []string) {
  var success bool = false
  var res = []string{}

  rec, err := face.NewRecognizer(MODELS_DIR)
  lib.LogError(err)
  defer rec.Close()

  rec.SetSamples(descriptors, positions)

  faces, err := rec.RecognizeFile(fullPath)
  lib.LogError(err)

  count := len(faces)

  if err != nil {
    fmt.Println("Cannot recognize: ", err)
  } else if count == 0 {
    lib.PrintMessage("No faces found in photo")
  } else {
    success = true

    for _, f := range faces {
      faceIndex := rec.Classify(f.Descriptor)
      name := names[faceIndex]
      res = append(res, name)
    }
  }
  return success, res
}
